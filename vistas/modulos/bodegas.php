

<div class="content-wrapper">

  <section class="content-header">

    <h1>

      Administrar bodegas

    </h1>

    <ol class="breadcrumb">

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Administrar bodegas</li>

    </ol>
<div></div>
  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">

        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarBodega">

          Agregar bodega

        </button>

      </div>

      <div class="box-body">

       <table class="table table-bordered table-striped dt-responsive tablas">

          <thead>

           <tr>

             <th style="width:10px">#</th>
             <th>Bodega</th>
             <th>Acciones</th>

           </tr>

          </thead>

          <tbody>

            <?php

              $item = null;
              $valor = null;

              $bodegas = ControladorBodegas::ctrMostrarBodegas($item, $valor);

              foreach ($bodegas as $key => $value) {

                echo ' <tr>

                        <td>'.($key+1).'</td>

                        <td>'.$value["bodega"].'</td>

                        <td>

                          <div class="btn-group">

                            <button class="btn btn-warning btnEditarBodega" idBodega="'.$value["id"].'"
                            data-toggle="modal" data-target="#modalEditarBodega"><i class="fa fa-pencil"></i></button>';

                            if($_SESSION["perfil"] == "Administrador"){

                              echo '<button class="btn btn-danger btnEliminarBodega" idBodega="'.$value["id"].'"><i class="fa fa-times"></i></button>';

                            }

                          echo '</div>

                        </td>

                      </tr>';
              }

            ?>

          </tbody>

       </table>

      </div>

    </div>

  </section>

</div>





<!--=====================================
MODAL AGREGAR BODEGA
======================================-->

<div id="modalAgregarBodega" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Agregar bodega</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-cubes"></i></span>

                <input type="text" class="form-control input-lg" name="nuevaBodega" placeholder="Ingresar bodega" required>

              </div>

            </div>

          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar bodega</button>

        </div>

        <?php

          $crearBodega = new ControladorBodegas();
          $crearBodega -> ctrCrearBodega();

        ?>


      </form>

    </div>

  </div>

</div>



<!--=====================================
MODAL EDITAR BODEGA
======================================-->


<div id="modalEditarBodega" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#3c8dbc; color:white">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Editar bodega</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!-- ENTRADA PARA EL NOMBRE -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <input type="text" class="form-control input-lg" name="editarBodega" id="editarBodega" required>

                 <input type="hidden"  name="idBodega" id="idBodega" required>

              </div>

            </div>

          </div>

        </div>

        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Salir</button>

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

      <?php

          $editarBodega = new ControladorBodegas();
          $editarBodega -> ctrEditarBodega();

        ?>

      </form>

    </div>

  </div>

</div>

<?php

$borrarBodega = new ControladorBodegas();
$borrarBodega -> ctrBorrarBodega();


?>
