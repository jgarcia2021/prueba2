<div class="content-wrapper">

  <section class="content-header">

    <h1>

      ENTRADA DE ALMACÉN

    </h1>

    <ol class="breadcrumb">

      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Entrada de Almacén</li>

    </ol>

  </section>

  <section class="content">

    <div class="row">

      <!--=====================================
      EL FORMULARIO
      ======================================-->

      <div class="col-lg-5 col-xs-12">

        <div class="box box-success">

          <div class="box-header with-border"></div>

          <form role="form" metohd="post">

            <div class="box-body">

              <div class="box">

                <!--=====================================
                ENTRADA DE LA FECHA INGRESO
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                    <input type="text" class="form-control pull-right" id="nuevoIngreso" name="ingreso" placeholder="Fecha de Ingreso">

                  </div>

                </div>




                <!--=====================================
                ENTRADA DEL CLIENTE
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-user-secret"></i></span>

                    <select class="form-control" id="seleccionarCliente" name="seleccionarCliente" required>

                    <option value="">-- Seleccionar cliente --</option>

                  </select>

                  </div>

                </div>


                <!--=====================================
                ENTRADA DEL PRODUCTO
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-cart-plus"></i></span>

                    <select class="form-control" id="seleccionarProducto" name="seleccionarProducto" required>

                    <option value="">-- Seleccionar producto --</option>

                    </select>

                  </div>

                </div>

                <!--=====================================
                ENTRADA DEL VAGÓN
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-train"></i></span>

                    <input type="text" class="form-control" id="nuevoVagon" name="entrada" placeholder="Ingresa No. Vagón">


                  </div>

                </div>



                <!--=====================================
                ENTRADA DE LA BODEGA
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-cubes"></i></span>

                    <select class="form-control" id="seleccionarBodega" name="seleccionarBodega" required>

                    <option value="">-- Seleccionar Bodega --</option>

                    </select>

                  </div>

                </div>


                <!--=====================================
                CANTIDAD DE ENTRADA
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-line-chart"></i></span>

                    <input type="text" class="form-control" id="nuevoEntrada" name="entrada" placeholder="Cantidad">

                    <span class="input-group-addon">Toneladas</span>


                  </div>

                </div>



                <!--=====================================
                ENTRADA DE COMENTARIOS
                ======================================-->

                <div class="form-group">

                  <div class="input-group">

                    <span class="input-group-addon"><i class="fa fa-commenting"></i></span>

                    <input type="text" class="form-control" id="nuevoComentario" name="ingreso" placeholder="Comentarios">

                  </div>

                </div>


              </div>

          </div>

          <div class="box-footer">

            <button type="submit" class="btn btn-primary pull-right">CONFIRMAR ENTRADA</button>

          </div>

        </form>

        </div>

      </div>

      <!--=====================================
      LA TABLA DE BALANCE
      ======================================-->

      <div class="col-lg-7 hidden-md hidden-sm hidden-xs">

        <div class="box box-warning">

          <div class="box-header with-border"></div>

          <div class="box-body">

            <table class="table table-bordered table-striped dt-responsive tablas">

               <thead>

                 <tr>
                  <th style="width: 10px">#</th>
                  <th>Cliente</th>
                  <th>Producto</th>
                  <th>vagón</th>
                  <th>Toneladas</th>
                  <th>Cargo Almacenaje</th>
                </tr>

              </thead>

              <tbody>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>Alorhum</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2345</td>
                  <td>92</td>
                  <td>$5,000.00</td>
                </tr>

                <tr>
                  <td>1.</td>
                  <td>KT-Clay</td>
                  <td>Alumina Calcinada</td>
                  <td>KTW-2678</td>
                  <td>92</td>
                  <td>$4,000.00</td>
                </tr>

              </tbody>

            </table>

          </div>

        </div>


      </div>

    </div>

  </section>

</div>
