<aside class="main-sidebar">

	 <section class="sidebar">

		<ul class="sidebar-menu">




			<li class="active">

				<a href="inicio">

					<i class="fa fa-home"></i>
					<span>Inicio</span>

				</a>

			</li>




			<li>

				<a href="usuarios">

					<i class="fa fa-users"></i>
					<span>Usuarios</span>

				</a>

			</li>



			<li>

				<a href="bodegas">

					<i class="fa fa-cubes"></i>
					<span>Bodegas</span>

				</a>

			</li>




			<li>

				<a href="categorias">

					<i class="fa fa-th"></i>
					<span>Categorías</span>

				</a>

			</li>





			<li>

				<a href="clientes">

					<i class="fa fa-user-secret"></i>
					<span>Clientes</span>

				</a>

			</li>





			<li>

				<a href="productos">

					<i class="fa fa-cart-plus"></i>
					<span>Productos</span>

				</a>

			</li>






			<li class="treeview">

				<a href="">

					<i class="fa fa-exchange"></i>

					<span>Operaciones</span>

					<span class="pull-right-container">

						<i class="fa fa-angle-left pull-right"></i>

					</span>

				</a>

				<ul class="treeview-menu">

					<li>

						<a href="entrada-almacen">

							<i class="fa fa-train"></i>
							<span>Entrada de Almacén</span>

						</a>

					</li>

					<li>

						<a href="salida-almacen">

							<i class="fa fa-truck"></i>
							<span>Salida de Almacén</span>

						</a>

					</li>

					<li>

						<a href="notificaciones">

							<i class="fa fa-bell"></i>
							<span>Notificación a Clientes</span>

						</a>

					</li>

				</ul>

			</li>






			<li class="treeview">

				<a href="">

					<i class="fa fa-bar-chart"></i>

					<span>Inventario</span>

					<span class="pull-right-container">

						<i class="fa fa-angle-left pull-right"></i>

					</span>

				</a>

				<ul class="treeview-menu">

					<li>

						<a href="balance">

							<i class="fa fa-pie-chart"></i>
							<span>Balance de inventario</span>

						</a>

					</li>

					<li>

						<a href="transacciones">

							<i class="fa fa-retweet"></i>
							<span>Transacciones</span>

						</a>

					</li>

				</ul>

			</li>





			<li>

				<a href="configuracion">

					<i class="fa  fa-gears"></i>
					<span>Configuración</span>

				</a>

			</li>




		</ul>

	 </section>

</aside>
