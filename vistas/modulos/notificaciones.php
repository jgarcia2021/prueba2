<div class="content-wrapper">

  <section class="content-header">

    <h1>

      NOTIFICACIÓN A CLIENTES

    </h1>

    <ol class="breadcrumb">

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Notificación a clientes</li>

    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">

        <button class="btn btn-warning" data-toggle="modal" data-target="#modalAgregarUsuario">

          Notificar

        </button>

      </div>

      <div class="box-body">

       <table class="table table-bordered table-striped dt-responsive tablas">

        <thead>

         <tr>

           <th style="width:5px"></th>
           <th style="width:10px">#</th>
           <th>Cliente</th>
           <th>Total TN</th>
           <th>TN S/Cargo</th>
           <th>TN C/Cargo</th>
           <th>Cargo</th>

         </tr>

        </thead>

        <tbody>

          <tr>
            <td><input type="checkbox" name="checknotifi"></td>
            <td>1</td>
            <td>Alorhum</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>$0.00</td>

          </tr>


          <tr>
            <td><input type="checkbox" name="checknotifi"></td>
            <td>2</td>
            <td>Alorhum</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>$0.00</td>

          </tr>


          <tr>
            <td><input type="checkbox" name="checknotifi"></td>
            <td>3</td>
            <td>Alorhum</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>$0.00</td>

          </tr>


          <tr>
            <td><input type="checkbox" name="checknotifi"></td>
            <td>4</td>
            <td>Alorhum</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>0.00</td>
            <td>$0.00</td>

          </tr>


        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>
