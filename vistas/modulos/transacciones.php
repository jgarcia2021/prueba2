<div class="content-wrapper">

  <section class="content-header">

    <h1>

      TRANSACCIONES REGISTRADAS

    </h1>

    <ol class="breadcrumb">

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Transacciones registradas</li>

    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">

        <button type="button" class="btn btn-warning" id="">

          <span>
            <i class="fa fa-calendar"></i> Rango de fecha
          </span>

          <i class="fa fa-caret-down"></i>

        </button>


        <a href="balance"><button class="btn btn-primary">Balance de inventario</button></a>



        <button class="btn btn-success">Exportar a Excel</button>




      </div>


      <div class="box-body">

       <table class="table table-bordered table-striped dt-responsive tablas">

        <thead>

         <tr>


           <th style="width:10px">#</th>
           <th>Cliente</th>
           <th>Producto</th>
           <th>Vagón</th>
           <th>Bodega</th>
           <th>Total</th>
           <th>Cargo</th>

         </tr>

        </thead>

        <tbody>

          <tr>

            <td>1</td>
            <td>Cato</td>
            <td>Arcilla CB</td>
            <td>KWT-462029</td>
            <td>Bodega 5</td>
            <td>85.81</td>
            <td>$6,693.18</td>

          </tr>


          <tr>

            <td>1</td>
            <td>Cato</td>
            <td>Arcilla CB</td>
            <td>KWT-462029</td>
            <td>Bodega 5</td>
            <td>85.81</td>
            <td>$6,693.18</td>

          </tr>


          <tr>

            <td>1</td>
            <td>Cato</td>
            <td>Arcilla CB</td>
            <td>KWT-462029</td>
            <td>Bodega 5</td>
            <td>85.81</td>
            <td>$6,693.18</td>

          </tr>


          <tr>

            <td>1</td>
            <td>Cato</td>
            <td>Arcilla CB</td>
            <td>KWT-462029</td>
            <td>Bodega 5</td>
            <td>85.81</td>
            <td>$6,693.18</td>

          </tr>


        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>
