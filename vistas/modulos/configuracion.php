<div class="content-wrapper">

  <section class="content-header">

    <h1>

      Parámetros de Operación

    </h1>

    <ol class="breadcrumb">

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Parámetros de Operación</li>

    </ol>

  </section>

  <br>


    <section class="content">



      <h4>Actualice la configuración de operación del sistema</h4>


      <!-- ENTRADA DE ALAMCENAJE INCLUIDO -->

      <div class="form-group">

        <div class="input-group">

          <span class="input-group-addon"><i class="fa fa-hourglass-1"></i></span>

          <input type="text" class="form-control input-lg" name="nuevodiaalmacenaje" placeholder="Ingresar Dias de Almacenaje Incluido" required>

        </div>

      </div>



    <!-- ENTRADA DE TARIFA POR TONELADA -->

    <div class="form-group">

      <div class="input-group">

        <span class="input-group-addon"><i class="fa  fa-dollar"></i></span>

        <input type="text" class="form-control input-lg" name="nuevotarifaalmacenaje" placeholder="Ingresar Tarifa por Tonelada" required>

      </div>

    </div>

    <br>

      <button class="btn btn-primary">Actualizar Parametros</button>

      <a href="inicio"><button class="btn btn-success">Inicio</button></a>


    </section>

</div>
