/*=============================================
EDITAR BODEGA
=============================================*/
$(".tablas").on("click", ".btnEditarBodega", function(){

	var idBodega = $(this).attr("idBodega");

	var datos = new FormData();
	datos.append("idBodega", idBodega);

	$.ajax({
		url: "ajax/bodegas.ajax.php",
		method: "POST",
      	data: datos,
      	cache: false,
     	contentType: false,
     	processData: false,
     	dataType:"json",
     	success: function(respuesta){

     		$("#editarBodega").val(respuesta["bodega"]);
     		$("#idBodega").val(respuesta["id"]);

     	}

	})


})





/*=============================================
ELIMINAR CATEGORIA
=============================================*/
$(".tablas").on("click", ".btnEliminarBodega", function(){

	 var idBodega= $(this).attr("idBodega");

	 swal({
	 	title: '¿Está seguro de borrar la bodega?',
	 	text: "¡Si no lo está puede cancelar la acción!",
	 	type: 'warning',
	 	showCancelButton: true,
	 	confirmButtonColor: '#3085d6',
	 	cancelButtonColor: '#d33',
	 	cancelButtonText: 'Cancelar',
	 	confirmButtonText: 'Si, borrar bodega!'
	 }).then(function(result){

	 	if(result.value){

	 		window.location = "index.php?ruta=bodegas&idBodega="+idBodega;

	 	}

	 })

})
