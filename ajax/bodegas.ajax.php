<?php

require_once "../controladores/bodegas.controlador.php";
require_once "../modelos/bodegas.modelo.php";

class AjaxBodegas{

	/*=============================================
	EDITAR BODEGA
	=============================================*/

	public $idBodega;

	public function ajaxEditarBodega(){

		$item = "id";
		$valor = $this->idBodega;

		$respuesta = ControladorBodegas::ctrMostrarBodegas($item, $valor);

		echo json_encode($respuesta);

	}
}

/*=============================================
EDITAR BODEGA
=============================================*/
if(isset($_POST["idBodega"])){

	$bodega = new AjaxBodegas();
	$bodega -> idBodega = $_POST["idBodega"];
	$bodega -> ajaxEditarBodega();
}
