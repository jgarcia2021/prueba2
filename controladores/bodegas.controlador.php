<?php

class ControladorBodegas{

	/*=============================================
	CREAR CATEGORIAS
	=============================================*/

	static public function ctrCrearBodega(){

		if(isset($_POST["nuevaBodega"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevaBodega"])){

				$tabla = "bodegas";

				$datos = $_POST["nuevaBodega"];

				$respuesta = ModeloBodegas::mdlIngresarBodega($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La bodega ha sido guardada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "bodegas";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La bodega no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "bodegas";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	MOSTRAR CATEGORIAS
	=============================================*/

	static public function ctrMostrarBodegas($item, $valor){

		$tabla = "bodegas";

		$respuesta = ModeloBodegas::mdlMostrarBodegas($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	EDITAR CATEGORIA
	=============================================*/

	static public function ctrEditarBodega(){

		if(isset($_POST["editarBodega"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarBodega"])){

				$tabla = "bodegas";

				$datos = array("bodega"=>$_POST["editarBodega"],
							   "id"=>$_POST["idBodega"]);

				$respuesta = ModeloBodegas::mdlEditarBodega($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "La bodega ha sido cambiada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "bodegas";

									}
								})

					</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡La bodega no puede ir vacía o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "bodegas";

							}
						})

			  	</script>';

			}

		}

	}

	/*=============================================
	BORRAR CATEGORIA
	=============================================*/

	static public function ctrBorrarBodega(){

		if(isset($_GET["idBodega"])){

			$tabla ="Bodegas";
			$datos = $_GET["idBodega"];

			$respuesta = ModeloBodegas::mdlBorrarBodega($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

					swal({
						  type: "success",
						  title: "La bodega ha sido borrada correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "bodegas";

									}
								})

					</script>';
			}
		}

	}
}
