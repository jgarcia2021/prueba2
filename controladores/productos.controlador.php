<?php

class ControladorProductos{

	/*=============================================
	CREAR PRODUCTO
	=============================================*/

	static public function ctrCrearProducto(){

		if(isset($_POST["nuevoProducto"])){

			if(preg_match('/^[0-9 ]+$/', $_POST["nuevoCliente"]) &&
			   preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoProducto"]) &&
			   preg_match('/^[0-9 ]+$/', $_POST["nuevaCategoria"])){

			   	$tabla = "productos";

			   	$datos = array("id_cliente"=>$_POST["nuevoCliente"],
					                 "producto"=>$_POST["nuevoProducto"],
					             "id_categoria"=>$_POST["nuevaCategoria"]);

			   	$respuesta = ModeloProductos::mdlIngresarProducto($tabla, $datos);

			   	if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El producto ha sido guardado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "productos";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El producto no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "productos";

							}
						})

			  	</script>';



			}

		}

	}

	/*=============================================
	MOSTRAR PRODUCTO
	=============================================*/

	static public function ctrMostrarProductos($item, $valor){

		$tabla = "productos";/*Cambio clientes por productos*/

		$respuesta = ModeloProductos::mdlMostrarProductos($tabla, $item, $valor);

		return $respuesta;

	}

	/*=============================================
	EDITAR PRODUCTO
	=============================================*/

	static public function ctrEditarProducto(){

		if(isset($_POST["editarProducto"])){

			if(preg_match('/^[0-9 ]+$/', $_POST["editarCliente"]) &&
			   preg_match('/^[-.,\-a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarProducto"]) &&
			   preg_match('/^[0-9 ]+$/', $_POST["editarCategoria"])){

			   	$tabla = "productos";

			   	$datos = array("id_cliente"=>$_POST["idCliente"],
			   				           "producto"=>$_POST["editarCliente"],
					             "id_categoria"=>$_POST["editarContacto"]);

			   	$respuesta = ModeloProductos::mdlEditarProducto($tabla, $datos);

			   	if($respuesta == "ok"){

					echo'<script>

					swal({
						  type: "success",
						  title: "El cliente ha sido cambiado correctamente",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
									if (result.value) {

									window.location = "clientes";

									}
								})

					</script>';

				}

			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El cliente no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "clientes";

							}
						})

			  	</script>';



			}

		}

	}

	/*=============================================
	ELIMINAR PRODUCTO
	=============================================*/

	static public function ctrBorrarProducto(){

		if(isset($_GET["idProducto"])){

			$tabla ="productos";
			$datos = $_GET["idProducto"];

			$respuesta = ModeloProductos::mdlEliminarProducto($tabla, $datos);

			if($respuesta == "ok"){

				echo'<script>

				swal({
					  type: "success",
					  title: "Esta seguro de borrar este producto?",
					  showConfirmButton: true,
					  confirmButtonText: "Cerrar",
					  closeOnConfirm: false
					  }).then(function(result){
								if (result.value) {

								window.location = "productos";

								}
							})

				</script>';

			}

		}

	}

}
